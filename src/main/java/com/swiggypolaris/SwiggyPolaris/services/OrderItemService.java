package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.OrderItemDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.OrderItem;

public interface OrderItemService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    OrderItemDto create(OrderItemDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    OrderItemDto update(OrderItemDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    OrderItemDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<OrderItemDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<OrderItemDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    OrderItem mapToEntity(OrderItemDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    OrderItemDto mapToDto(OrderItem type);

}
