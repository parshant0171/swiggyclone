package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.OrderDto;
import com.swiggypolaris.SwiggyPolaris.dtos.OrderItemDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Order;

public interface OrderService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    OrderDto create(OrderDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    OrderDto update(OrderDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    OrderDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<OrderDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<OrderDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    Order mapToEntity(OrderDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    OrderDto mapToDto(Order type);

    // Method to add the Create Order with OrderItems
    OrderDto addOrderItem(OrderItemDto orderItemDto);
}
