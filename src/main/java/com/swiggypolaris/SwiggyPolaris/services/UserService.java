package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.UserDto;
import com.swiggypolaris.SwiggyPolaris.dtos.UserResponseDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.User;

import java.util.List;

public interface UserService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    UserDto create(UserDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    UserDto update(UserDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    UserResponseDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<UserResponseDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<UserResponseDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    User mapToEntity(UserDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    UserDto mapToDto(User type);

    // It will return User Response Dto : Map Incoming User Entity to UserResponseDto
    UserResponseDto mapToUserResponseDto(User Type);

    // Get single user by id
    UserDto getUserById(Long id);

    // Get single user by email
    UserDto getUserByEmail(String email);

    // Search user
    List<UserDto> searchUser(String keyword);

}
