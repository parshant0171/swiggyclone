package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.RestaurantHasMenuDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.RestaurantHasMenu;

public interface RestaurantHasMenuService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    RestaurantHasMenuDto create(RestaurantHasMenuDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    RestaurantHasMenuDto update(RestaurantHasMenuDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    RestaurantHasMenuDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<RestaurantHasMenuDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<RestaurantHasMenuDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    RestaurantHasMenu mapToEntity(RestaurantHasMenuDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    RestaurantHasMenuDto mapToDto(RestaurantHasMenu type);

}
