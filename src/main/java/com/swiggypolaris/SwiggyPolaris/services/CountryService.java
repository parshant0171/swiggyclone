package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.CountryDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Country;

import java.util.List;

public interface CountryService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    CountryDto create(CountryDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    CountryDto update(CountryDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    CountryDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<CountryDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<CountryDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    Country mapToEntity(CountryDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    CountryDto mapToDto(Country type);

}
