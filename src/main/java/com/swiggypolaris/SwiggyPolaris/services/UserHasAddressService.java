package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.UserHasAddressDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.UserHasAddress;

public interface UserHasAddressService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    UserHasAddressDto create(UserHasAddressDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    UserHasAddressDto update(UserHasAddressDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    UserHasAddressDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<UserHasAddressDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<UserHasAddressDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    UserHasAddress mapToEntity(UserHasAddressDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    UserHasAddressDto mapToDto(UserHasAddress type);

}
