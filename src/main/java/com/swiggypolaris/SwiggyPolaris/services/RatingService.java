package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.RatingDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Rating;

public interface RatingService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    RatingDto create(RatingDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    RatingDto update(RatingDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    RatingDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<RatingDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<RatingDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    Rating mapToEntity(RatingDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    RatingDto mapToDto(Rating type);

}
