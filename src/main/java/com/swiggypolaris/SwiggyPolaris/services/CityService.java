package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.CityDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.City;

public interface CityService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    CityDto create(CityDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    CityDto update(CityDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    CityDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<CityDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<CityDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    City mapToEntity(CityDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    CityDto mapToDto(City type);

}
