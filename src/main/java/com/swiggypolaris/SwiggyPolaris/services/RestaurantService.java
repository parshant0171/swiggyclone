package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.*;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Restaurant;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface RestaurantService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    RestaurantDto create(RestaurantDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    RestaurantDto update(RestaurantDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    RestaurantResponseDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<RestaurantResponseDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<RestaurantDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    Restaurant mapToEntity(RestaurantDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    RestaurantDto mapToDto(Restaurant type);

    // It will return Restaurant Response Dto : Map Incoming Response Entity to RestaurantResponseDto
    RestaurantResponseDto mapToRestaurantResponseDto(Restaurant Type);

    List<SuggestedRestaurantMenuDto> getSuggestedRestaurants(String foodType, int deliveryTimeInMinutes) throws IOException;

    Optional<RiderDto> findNearestRider(Long restaurantId);
}
