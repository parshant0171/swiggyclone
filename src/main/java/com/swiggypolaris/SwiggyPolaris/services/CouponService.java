package com.swiggypolaris.SwiggyPolaris.services;

import com.swiggypolaris.SwiggyPolaris.dtos.CouponDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Coupon;

public interface CouponService {


    /**
     * Create data.
     * Arguments : Contain parameter with type Dto
     * Method Declaration for creating a record in Entity.
     */
    CouponDto create(CouponDto dto);


    /**
     * Update data.
     * Arguments : Contain parameter with type Dto
     *            : id of the record to be updated
     * Method Declaration for updating a record in Entity.
     */
    CouponDto update(CouponDto dto, long id);

    /**
     * Delete data.
     * Arguments : id of the record to be deleted
     * Method Declaration for deleting a record in Entity.
     */
    void delete(long id);

    /**
     * show data.
     * Arguments : id of the record to be show
     * Method Declaration for showing a record in Entity.
     */
    CouponDto show(long id);

    /**
     * Get All data.
     * Arguments : Nothing
     * Method Declaration for showing all record in Entity.
     */
    PageableResponse<CouponDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<CouponDto> classDto);

    // Entity Mapper : Map Incoming Dto to Entity
    Coupon mapToEntity(CouponDto type);

    // Dto Mapper : Map Incoming Entity to Dto
    CouponDto mapToDto(Coupon type);

}
