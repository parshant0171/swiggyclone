package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.OrderDto;
import com.swiggypolaris.SwiggyPolaris.dtos.UserDto;
import com.swiggypolaris.SwiggyPolaris.dtos.UserHasAddressDto;
import com.swiggypolaris.SwiggyPolaris.dtos.UserResponseDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.User;
import com.swiggypolaris.SwiggyPolaris.repositories.UserRepository;
import com.swiggypolaris.SwiggyPolaris.services.UserService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    @Override
    public UserDto create(UserDto dto) {
        try {

            User entity = mapToEntity(dto);
            User savedEntity = userRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public UserDto update(UserDto dto, long id) {
        try {
            User existingEntity = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            User savedEntity = userRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            User entityToDelete = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            userRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public UserResponseDto show(long id) {
        try {
            User entity = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToUserResponseDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<UserResponseDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<UserResponseDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<User> page = userRepository.findAll(pageable);
            PageableResponse<UserResponseDto> response = Helper.getPageableResponse(page, UserResponseDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public User mapToEntity(UserDto dto) {
        User entity = modelMapper.map(dto, User.class);
        return entity;
    }

    @Override
    public UserDto mapToDto(User type) {
        UserDto dtoObject  = modelMapper.map(type, UserDto.class);
        return dtoObject;
    }

    @Override
    public UserResponseDto mapToUserResponseDto(User type) {
        UserResponseDto dtoObject = modelMapper.map(type, UserResponseDto.class);

        List<UserHasAddressDto> addressDtos = type.getAddresses().stream().map(address ->
                modelMapper.map(address, UserHasAddressDto.class)
        ).collect(Collectors.toList());
        dtoObject.setAddresses(addressDtos);

        // Map orders
        List<OrderDto> orderDtos = type.getOrders().stream().map(order ->
                modelMapper.map(order, OrderDto.class)
        ).collect(Collectors.toList());
        dtoObject.setOrders(orderDtos);

        return dtoObject;
    }

    @Override
    public UserDto getUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("user not found with given id !!"));
        return mapToDto(user);
    }

    @Override
    public UserDto getUserByEmail(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("User not found with given email id !!"));
        return mapToDto(user);
    }

    @Override
    public List<UserDto> searchUser(String keyword) {
        List<User> users = userRepository.findByNameContaining(keyword);
        List<UserDto> dtoList = users.stream().map(user -> mapToDto(user)).collect(Collectors.toList());
        return dtoList;
    }
}
