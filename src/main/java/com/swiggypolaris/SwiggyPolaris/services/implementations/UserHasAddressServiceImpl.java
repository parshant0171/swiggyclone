package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.UserHasAddressDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.UserHasAddress;
import com.swiggypolaris.SwiggyPolaris.repositories.UserHasAddressRepository;
import com.swiggypolaris.SwiggyPolaris.services.UserHasAddressService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;

@Service
public class UserHasAddressServiceImpl implements UserHasAddressService {

    @Autowired
    private UserHasAddressRepository userHasAddressRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(UserHasAddressServiceImpl.class);

    @Override
    public UserHasAddressDto create(UserHasAddressDto dto) {
        try {
            UserHasAddress entity = mapToEntity(dto);
            UserHasAddress savedEntity = userHasAddressRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public UserHasAddressDto update(UserHasAddressDto dto, long id) {
        try {
            UserHasAddress existingEntity = userHasAddressRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            UserHasAddress savedEntity = userHasAddressRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            UserHasAddress entityToDelete = userHasAddressRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            userHasAddressRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public UserHasAddressDto show(long id) {
        try {
            UserHasAddress entity = userHasAddressRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<UserHasAddressDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<UserHasAddressDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<UserHasAddress> page = userHasAddressRepository.findAll(pageable);
            PageableResponse<UserHasAddressDto> response = Helper.getPageableResponse(page, UserHasAddressDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public UserHasAddress mapToEntity(UserHasAddressDto dto) {
        UserHasAddress entity = modelMapper.map(dto, UserHasAddress.class);
        return entity;
    }

    @Override
    public UserHasAddressDto mapToDto(UserHasAddress type) {
        UserHasAddressDto dtoObject  = modelMapper.map(type, UserHasAddressDto.class);
        return dtoObject;
    }
}
