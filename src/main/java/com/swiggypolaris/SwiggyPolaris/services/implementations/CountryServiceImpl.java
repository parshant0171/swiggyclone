package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.CountryDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Country;
import com.swiggypolaris.SwiggyPolaris.repositories.CountryRepository;
import com.swiggypolaris.SwiggyPolaris.services.CountryService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);

    @Override
    public CountryDto create(CountryDto dto) {
        try {
            Country entity = mapToEntity(dto);
            Country savedEntity = countryRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public CountryDto update(CountryDto dto, long id) {
        try {
            Country existingEntity = countryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            Country savedEntity = countryRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            Country entityToDelete = countryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            countryRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public CountryDto show(long id) {
        try {
            Country entity = countryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<CountryDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<CountryDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<Country> page = countryRepository.findAll(pageable);
            PageableResponse<CountryDto> response = Helper.getPageableResponse(page, CountryDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public Country mapToEntity(CountryDto dto) {
        Country entity = modelMapper.map(dto, Country.class);
        return entity;
    }

    @Override
    public CountryDto mapToDto(Country type) {
        CountryDto dtoObject  = modelMapper.map(type, CountryDto.class);
        return dtoObject;
    }

}
