package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.DeliveryStatusDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.DeliveryStatus;
import com.swiggypolaris.SwiggyPolaris.repositories.DeliveryStatusRepository;
import com.swiggypolaris.SwiggyPolaris.services.DeliveryStatusService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;

@Service
public class DeliveryStatusServiceImpl implements DeliveryStatusService {

    @Autowired
    private DeliveryStatusRepository deliveryStatusRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(DeliveryStatusServiceImpl.class);

    @Override
    public DeliveryStatusDto create(DeliveryStatusDto dto) {
        try {
            DeliveryStatus entity = mapToEntity(dto);
            DeliveryStatus savedEntity = deliveryStatusRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public DeliveryStatusDto update(DeliveryStatusDto dto, long id) {
        try {
            DeliveryStatus existingEntity = deliveryStatusRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            DeliveryStatus savedEntity = deliveryStatusRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            DeliveryStatus entityToDelete = deliveryStatusRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            deliveryStatusRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public DeliveryStatusDto show(long id) {
        try {
            DeliveryStatus entity = deliveryStatusRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<DeliveryStatusDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<DeliveryStatusDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<DeliveryStatus> page = deliveryStatusRepository.findAll(pageable);
            PageableResponse<DeliveryStatusDto> response = Helper.getPageableResponse(page, DeliveryStatusDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public DeliveryStatus mapToEntity(DeliveryStatusDto dto) {
        DeliveryStatus entity = modelMapper.map(dto, DeliveryStatus.class);
        return entity;
    }

    @Override
    public DeliveryStatusDto mapToDto(DeliveryStatus type) {
        DeliveryStatusDto dtoObject  = modelMapper.map(type, DeliveryStatusDto.class);
        return dtoObject;
    }
}
