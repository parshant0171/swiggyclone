package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.StateDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.State;
import com.swiggypolaris.SwiggyPolaris.repositories.StateRepository;
import com.swiggypolaris.SwiggyPolaris.services.StateService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;

@Service
public class StateServiceImpl implements StateService {

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(StateServiceImpl.class);

    @Override
    public StateDto create(StateDto dto) {
        try {
            State entity = mapToEntity(dto);
            State savedEntity = stateRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public StateDto update(StateDto dto, long id) {
        try {
            State existingEntity = stateRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            State savedEntity = stateRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            State entityToDelete = stateRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            stateRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public StateDto show(long id) {
        try {
            State entity = stateRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<StateDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<StateDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<State> page = stateRepository.findAll(pageable);
            PageableResponse<StateDto> response = Helper.getPageableResponse(page, StateDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public State mapToEntity(StateDto dto) {
        State entity = modelMapper.map(dto, State.class);
        return entity;
    }

    @Override
    public StateDto mapToDto(State type) {
        StateDto dtoObject  = modelMapper.map(type, StateDto.class);
        return dtoObject;
    }

}
