package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.OrderDto;
import com.swiggypolaris.SwiggyPolaris.dtos.OrderItemDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Order;
import com.swiggypolaris.SwiggyPolaris.models.OrderItem;
import com.swiggypolaris.SwiggyPolaris.models.RestaurantHasMenu;
import com.swiggypolaris.SwiggyPolaris.repositories.*;
import com.swiggypolaris.SwiggyPolaris.services.OrderService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private DeliveryStatusRepository deliveryStatusRepository;

    @Autowired
    private RiderRepository riderRepository;

    @Autowired
    private RestaurantHasMenuRepository restaurantHasMenuRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Override
    public OrderDto create(OrderDto dto) {
        try {
            Order entity = mapToEntity(dto);
            Order savedEntity = orderRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public OrderDto update(OrderDto dto, long id) {
        try {
            Order existingEntity = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            Order savedEntity = orderRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            Order entityToDelete = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            orderRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public OrderDto show(long id) {
        try {
            Order entity = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<OrderDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<OrderDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<Order> page = orderRepository.findAll(pageable);
            PageableResponse<OrderDto> response = Helper.getPageableResponse(page, OrderDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public Order mapToEntity(OrderDto dto) {
        Order entity = modelMapper.map(dto, Order.class);
        return entity;
    }

    @Override
    public OrderDto mapToDto(Order type) {
        OrderDto dtoObject  = modelMapper.map(type, OrderDto.class);
        return dtoObject;
    }

    // Method to Create Order with the Order Items selected by User from restaurant's menu
    @Override
    public OrderDto addOrderItem(OrderItemDto orderItemDto) {

        // Get the restaurantHasMenu to retrieve the restaurant
        RestaurantHasMenu restaurantHasMenu = restaurantHasMenuRepository.findById(orderItemDto.getRestaurantHasMenuId())
                .orElseThrow(() -> new RuntimeException("Menu item not found"));

        long restaurantId = restaurantHasMenu.getRestaurant().getId();

        // Check if there's an existing order with isAccepted as false
        long userId = 1; // Temp Data, moving forward we will add the userId from Auth

        // Check if there is any Order which is not accepted and created with order items
        Optional<Order> existingOrderOpt = orderRepository.findByUserIdAndIsAcceptedFalse(1);

        Order order;

        if (existingOrderOpt.isPresent()) {
            order = existingOrderOpt.get();
        } else {
            // Create a new order
            order = new Order();
            order.setUser(userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found")));
            order.setRestaurant(restaurantRepository.findById(restaurantId).orElseThrow(() -> new RuntimeException("Restaurant not found")));
            order.setOrderTotal(0.0); // Initially Set to 0
            order.setDeliveryStatus(deliveryStatusRepository.findByStatusName("Pending").orElseThrow(() -> new RuntimeException("Delivery Status not found")));
            order.setRider(null); // Initially null, when accepted we will update the rider and Status
            order.setAccepted(false); // Initially False

            // We will calculate the estimation Time Logic Explanation in Function
            long estimatedDeliveryTime = calculateEstimatedDeliveryTime(restaurantId, userId);
            order.setEstimatedDeliveryTime(estimatedDeliveryTime);
            order.setActualDeliveryTime(estimatedDeliveryTime);

            order = orderRepository.save(order);
        }

        // Create the order item
        OrderItem orderItem = new OrderItem();
        orderItem.setOrder(order);
        orderItem.setRestaurantHasMenu(restaurantHasMenuRepository.findById(orderItemDto.getRestaurantHasMenuId()).orElseThrow(() -> new RuntimeException("Menu item not found")));
        orderItem.setQuantity(orderItemDto.getQuantity());
        order.getOrderItems().add(orderItem);
        orderItem = orderItemRepository.save(orderItem);

        // Update the order total
        updateOrderTotal(order);

        // Return the updated order as DTO
        return orderToDto(order);
    }

    // Method to update the OrderTotal
    /* will be updated on the basis of the price from restaurant has Menu
     * and the quantity from order items table
    *
    * */
    private void updateOrderTotal(Order order) {
        double orderTotal = order.getOrderItems().stream()
                .mapToDouble(orderItem -> orderItem.getQuantity() * orderItem.getRestaurantHasMenu().getPrice())
                .sum();
        order.setOrderTotal(orderTotal);
        orderRepository.save(order);
    }

    // Method to calculate the estimated Delivery Time
    /* we will calculate the time by using this concept
     * 1. Rider time to reach to restaurant - r1
     * 2. Time to cook the food - ct
     * 3. Rider's time from restaurant to user's location - r2
     *
     * Now Actual Time = greaterOf(r1,ct) + r2
     *
     * */
    private long calculateEstimatedDeliveryTime(long restaurantId, long userId) {
        // Logic to calculate estimated delivery time
        return 0;
    }


    // Mapper Function to convert the Order To OrderDto
    private OrderDto orderToDto(Order order) {
        return OrderDto.builder()
                .id(order.getId())
                .userId(order.getUser().getId())
                .restaurantId(order.getRestaurant().getId())
                .orderTotal(order.getOrderTotal())
                .deliveryStatusId(order.getDeliveryStatus().getId())
                .riderId(order.getRider() != null ? order.getRider().getId() : 0)
                .isAccepted(order.isAccepted())
                .estimatedDeliveryTime(order.getEstimatedDeliveryTime())
                .actualDeliveryTime(order.getActualDeliveryTime())
                .createdAt(order.getCreatedAt())
                .updatedAt(order.getUpdatedAt())
                .build();
    }
}
