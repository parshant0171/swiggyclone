package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.RiderDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Rider;
import com.swiggypolaris.SwiggyPolaris.repositories.RiderRepository;
import com.swiggypolaris.SwiggyPolaris.services.RiderService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;

@Service
public class RiderServiceImpl implements RiderService {

    @Autowired
    private RiderRepository riderRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(RiderServiceImpl.class);

    @Override
    public RiderDto create(RiderDto dto) {
        try {
            Rider entity = mapToEntity(dto);
            Rider savedEntity = riderRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public RiderDto update(RiderDto dto, long id) {
        try {
            Rider existingEntity = riderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            Rider savedEntity = riderRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            Rider entityToDelete = riderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            riderRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public RiderDto show(long id) {
        try {
            Rider entity = riderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<RiderDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<RiderDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<Rider> page = riderRepository.findAll(pageable);
            PageableResponse<RiderDto> response = Helper.getPageableResponse(page, RiderDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public Rider mapToEntity(RiderDto dto) {
        Rider entity = modelMapper.map(dto, Rider.class);
        return entity;
    }

    @Override
    public RiderDto mapToDto(Rider type) {
        RiderDto dtoObject  = modelMapper.map(type, RiderDto.class);
        return dtoObject;
    }
}
