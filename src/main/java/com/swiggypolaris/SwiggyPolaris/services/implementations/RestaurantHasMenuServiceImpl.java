package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.RestaurantHasMenuDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.RestaurantHasMenu;
import com.swiggypolaris.SwiggyPolaris.repositories.RestaurantHasMenuRepository;
import com.swiggypolaris.SwiggyPolaris.services.RestaurantHasMenuService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;

@Service
public class RestaurantHasMenuServiceImpl implements RestaurantHasMenuService {

    @Autowired
    private RestaurantHasMenuRepository restaurantHasMenuRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(RestaurantHasMenuServiceImpl.class);

    @Override
    public RestaurantHasMenuDto create(RestaurantHasMenuDto dto) {
        try {
            RestaurantHasMenu entity = mapToEntity(dto);
            RestaurantHasMenu savedEntity = restaurantHasMenuRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public RestaurantHasMenuDto update(RestaurantHasMenuDto dto, long id) {
        try {
            RestaurantHasMenu existingEntity = restaurantHasMenuRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            RestaurantHasMenu savedEntity = restaurantHasMenuRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            RestaurantHasMenu entityToDelete = restaurantHasMenuRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            restaurantHasMenuRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public RestaurantHasMenuDto show(long id) {
        try {
            RestaurantHasMenu entity = restaurantHasMenuRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<RestaurantHasMenuDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<RestaurantHasMenuDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<RestaurantHasMenu> page = restaurantHasMenuRepository.findAll(pageable);
            PageableResponse<RestaurantHasMenuDto> response = Helper.getPageableResponse(page, RestaurantHasMenuDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public RestaurantHasMenu mapToEntity(RestaurantHasMenuDto dto) {
        RestaurantHasMenu entity = modelMapper.map(dto, RestaurantHasMenu.class);
        return entity;
    }

    @Override
    public RestaurantHasMenuDto mapToDto(RestaurantHasMenu type) {
        RestaurantHasMenuDto dtoObject  = modelMapper.map(type, RestaurantHasMenuDto.class);
        return dtoObject;
    }
}
