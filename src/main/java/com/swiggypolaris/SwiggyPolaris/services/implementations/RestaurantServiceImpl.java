package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggypolaris.SwiggyPolaris.dtos.*;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Restaurant;
import com.swiggypolaris.SwiggyPolaris.models.Rider;
import com.swiggypolaris.SwiggyPolaris.repositories.RestaurantRepository;
import com.swiggypolaris.SwiggyPolaris.repositories.RiderRepository;
import com.swiggypolaris.SwiggyPolaris.services.RestaurantService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RiderRepository riderRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(RestaurantServiceImpl.class);

    @Override
    public RestaurantDto create(RestaurantDto dto) {
        try {
            Restaurant entity = mapToEntity(dto);
            Restaurant savedEntity = restaurantRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public RestaurantDto update(RestaurantDto dto, long id) {
        try {
            Restaurant existingEntity = restaurantRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            Restaurant savedEntity = restaurantRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            Restaurant entityToDelete = restaurantRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            restaurantRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public RestaurantResponseDto show(long id) {
        try {
            Restaurant entity = restaurantRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToRestaurantResponseDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<RestaurantResponseDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<RestaurantDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<Restaurant> page = restaurantRepository.findAll(pageable);
            PageableResponse<RestaurantResponseDto> response = Helper.getPageableResponse(page, RestaurantResponseDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }


    @Override
    public Restaurant mapToEntity(RestaurantDto dto) {
        Restaurant entity = modelMapper.map(dto, Restaurant.class);
        return entity;
    }

    @Override
    public RestaurantDto mapToDto(Restaurant type) {
        RestaurantDto dtoObject  = modelMapper.map(type, RestaurantDto.class);
        return dtoObject;
    }

    @Override
    public RestaurantResponseDto mapToRestaurantResponseDto(Restaurant type) {
        RestaurantResponseDto dtoObject = modelMapper.map(type, RestaurantResponseDto.class);
        dtoObject.setCity(modelMapper.map(type.getCity(), CityDto.class));
        // Map menus
        List<RestaurantHasMenuDto> menuDtos = type.getRestaurantMenus().stream().map(menu ->
                modelMapper.map(menu, RestaurantHasMenuDto.class)
        ).collect(Collectors.toList());
        dtoObject.setMenus(menuDtos);

        return dtoObject;
    }

    @Override
    public List<SuggestedRestaurantMenuDto> getSuggestedRestaurants(String foodType, int deliveryTimeInMinutes) throws IOException {

        List<Object[]> results = restaurantRepository.findByFoodTypeAndDeliveryTime(foodType, deliveryTimeInMinutes);

        return results.stream().map(result ->
                SuggestedRestaurantMenuDto.builder()
                        .restaurantId((Long) result[0])
                        .restaurantName((String) result[1])
                        .contactNumber((Long) result[2])
                        .fullAddress((String) result[3])
                        .menuId((Long) result[4])
                        .itemName((String) result[5])
                        .price((Double) result[6])
                        .cookingTime((Integer) result[7])
                        .build()
        ).collect(Collectors.toList());
    }


    public Optional<RiderDto> findNearestRider(Long restaurantId) {
        Restaurant restaurant = restaurantRepository.findById(restaurantId)
                .orElseThrow(() -> new RuntimeException("Restaurant not found with id: " + restaurantId));

        List<Rider> availableRiders = riderRepository.findAllByStatus("Active");

        if (availableRiders.isEmpty()) {
            return Optional.empty();
        }

        Rider nearestRider = availableRiders.stream()
                .min((r1, r2) -> Double.compare(
                        distance(restaurant.getCurrentLocationLatitude(), restaurant.getCurrentLocationLongitude(), r1.getCurrentLocationLatitude(), r1.getCurrentLocationLongitude()),
                        distance(restaurant.getCurrentLocationLatitude(), restaurant.getCurrentLocationLongitude(), r2.getCurrentLocationLatitude(), r2.getCurrentLocationLongitude())))
                .orElse(null);

        if (nearestRider == null) {
            return Optional.empty();
        }

        return Optional.of(modelMapper.map(nearestRider,RiderDto.class));
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        final int R = 6371; // Radius of the Earth in km
        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c; // convert to meters
    }

}
