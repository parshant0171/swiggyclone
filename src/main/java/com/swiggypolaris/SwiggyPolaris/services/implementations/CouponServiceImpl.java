package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.CouponDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.Coupon;
import com.swiggypolaris.SwiggyPolaris.repositories.CouponRepository;
import com.swiggypolaris.SwiggyPolaris.services.CouponService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;

@Service
public class CouponServiceImpl implements CouponService {


    @Autowired
    private CouponRepository couponRespository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(CouponServiceImpl.class);

    @Override
    public CouponDto create(CouponDto dto) {
        try {
            Coupon entity = mapToEntity(dto);
            Coupon savedEntity = couponRespository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public CouponDto update(CouponDto dto, long id) {
        try {
            Coupon existingEntity = couponRespository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            Coupon savedEntity = couponRespository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            Coupon entityToDelete = couponRespository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            couponRespository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public CouponDto show(long id) {
        try {
            Coupon entity = couponRespository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<CouponDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<CouponDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<Coupon> page = couponRespository.findAll(pageable);
            PageableResponse<CouponDto> response = Helper.getPageableResponse(page, CouponDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public Coupon mapToEntity(CouponDto dto) {
        Coupon entity = modelMapper.map(dto, Coupon.class);
        return entity;
    }

    @Override
    public CouponDto mapToDto(Coupon type) {
        CouponDto dtoObject  = modelMapper.map(type, CouponDto.class);
        return dtoObject;
    }

}
