package com.swiggypolaris.SwiggyPolaris.services.implementations;

import com.swiggypolaris.SwiggyPolaris.dtos.CityDto;
import com.swiggypolaris.SwiggyPolaris.exceptions.ResourceNotFoundException;
import com.swiggypolaris.SwiggyPolaris.helpers.Helper;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.models.City;
import com.swiggypolaris.SwiggyPolaris.repositories.CityRepository;
import com.swiggypolaris.SwiggyPolaris.services.CityService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.lang.reflect.Field;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private ModelMapper modelMapper;

    private Logger logger = LoggerFactory.getLogger(CityServiceImpl.class);

    @Override
    public CityDto create(CityDto dto) {
        try {
            City entity = mapToEntity(dto);
            City savedEntity = cityRepository.save(entity);
            return mapToDto(savedEntity);

        } catch (Exception e) {
            throw new RuntimeException("Error mapping objects: " + e.getMessage());
        }
    }

    @Override
    public CityDto update(CityDto dto, long id) {
        try {
            City existingEntity = cityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));

            for (Field field : dto.getClass().getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("createdAt")) {

                    // Get the field value from the DTO using reflection
                    field.setAccessible(true);
                    Object value = field.get(dto);

                    // Set the field value in the existing entity using reflection
                    Field existingField = existingEntity.getClass().getDeclaredField(field.getName());
                    existingField.setAccessible(true);
                    existingField.set(existingEntity, value);
                }
            }

            // Save the updated entity to the repository
            City savedEntity = cityRepository.save(existingEntity);

            // Map saved entity back to DTO
            return mapToDto(savedEntity);
        } catch (Throwable e) {
            throw new RuntimeException("Error updating entity: " + e.getMessage());
        }
    }

    @Override
    public void delete(long id) {
        try {
            City entityToDelete = cityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            cityRepository.delete(entityToDelete);
        } catch (Throwable e) {
            throw new RuntimeException("Error deleting entity: " + e.getMessage());
        }
    }

    @Override
    public CityDto show(long id) {
        try {
            City entity = cityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Entity not found with ID: " + id));
            return mapToDto(entity);
        } catch (Throwable e) {
            throw new RuntimeException("Error retrieving entity: " + e.getMessage());
        }
    }

    @Override
    public PageableResponse<CityDto> index(int pageNumber, int pageSize, String sortBy, String sortDir, Class<CityDto> classDto) {
        try {
            Sort sort = (sortDir.equalsIgnoreCase("desc")) ? (Sort.by(sortBy).descending()) : (Sort.by(sortBy).ascending());
            Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
            Page<City> page = cityRepository.findAll(pageable);
            PageableResponse<CityDto> response = Helper.getPageableResponse(page, CityDto.class);
            return response;
        } catch (Exception e) {
            throw new RuntimeException("Error retrieving entities: " + e.getMessage());
        }
    }

    @Override
    public City mapToEntity(CityDto dto) {
        City entity = modelMapper.map(dto, City.class);
        return entity;
    }

    @Override
    public CityDto mapToDto(City type) {
        CityDto dtoObject  = modelMapper.map(type, CityDto.class);
        return dtoObject;
    }

}
