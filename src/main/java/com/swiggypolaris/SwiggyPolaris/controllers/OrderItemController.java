package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.OrderItemDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.OrderItemService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order-items")
public class OrderItemController {

    @Autowired
    private OrderItemService orderItemService;

    private Logger logger = LoggerFactory.getLogger(OrderItemController.class);

    // Create Order Item
    @PostMapping
    public ResponseEntity<OrderItemDto> create(
            @Valid
            @RequestBody OrderItemDto orderItemDto
    ) {
        try{
            OrderItemDto createdDto = orderItemService.create(orderItemDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Order Item
    @PutMapping("/{id}")
    public ResponseEntity<OrderItemDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody OrderItemDto orderItemDto
    ) {
        OrderItemDto updatedOrderItemDto = orderItemService.update(orderItemDto, id);
        return new ResponseEntity<>(updatedOrderItemDto, HttpStatus.OK);
    }

    // Delete Order Item
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        orderItemService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("Order Item is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<OrderItemDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(orderItemService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<OrderItemDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "id", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(orderItemService.index(pageNumber, pageSize, sortBy, sortDir,OrderItemDto.class), HttpStatus.OK);
    }
}
