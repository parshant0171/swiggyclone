package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.StateDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.StateService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/states")
public class StateController {
    @Autowired
    private StateService stateService;

    private Logger logger = LoggerFactory.getLogger(StateController.class);

    // Create State
    @PostMapping
    public ResponseEntity<StateDto> create(
            @Valid
            @RequestBody StateDto stateDto
    ) {
        try{
            StateDto createdDto = stateService.create(stateDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update State
    @PutMapping("/{id}")
    public ResponseEntity<StateDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody StateDto stateDto
    ) {
        StateDto updatedStateDto = stateService.update(stateDto, id);
        return new ResponseEntity<>(updatedStateDto, HttpStatus.OK);
    }

    // Delete State
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        stateService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("State is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<StateDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(stateService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<StateDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "stateName", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(stateService.index(pageNumber, pageSize, sortBy, sortDir,StateDto.class), HttpStatus.OK);
    }
}
