package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.CountryDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.CountryService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/countries")
public class CountryController {

    @Autowired
    private CountryService countryService;

    private Logger logger = LoggerFactory.getLogger(CountryController.class);

    // Create Country
    @PostMapping
    public ResponseEntity<CountryDto> create(
            @Valid
            @RequestBody CountryDto countryDto
    ) {
        try{
            CountryDto createdDto = countryService.create(countryDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Country
    @PutMapping("/{id}")
    public ResponseEntity<CountryDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody CountryDto countryDto
    ) {
        CountryDto updatedCountryDto = countryService.update(countryDto, id);
        return new ResponseEntity<>(updatedCountryDto, HttpStatus.OK);
    }

    // Delete Country
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        countryService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("Country is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<CountryDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(countryService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<CountryDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "countryName", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(countryService.index(pageNumber, pageSize, sortBy, sortDir,CountryDto.class), HttpStatus.OK);
    }
}
