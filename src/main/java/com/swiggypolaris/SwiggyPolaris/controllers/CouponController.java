package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.CouponDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.CouponService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/coupons")
public class CouponController {

    @Autowired
    private CouponService couponService;

    private Logger logger = LoggerFactory.getLogger(CouponController.class);

    // Create Coupon
    @PostMapping
    public ResponseEntity<CouponDto> create(
            @Valid
            @RequestBody CouponDto couponDto
    ) {
        try{
            CouponDto createdDto = couponService.create(couponDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Coupon
    @PutMapping("/{id}")
    public ResponseEntity<CouponDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody CouponDto couponDto
    ) {
        CouponDto updatedCouponDto = couponService.update(couponDto, id);
        return new ResponseEntity<>(updatedCouponDto, HttpStatus.OK);
    }

    // Delete Coupon
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        couponService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("Coupon is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<CouponDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(couponService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<CouponDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "code", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(couponService.index(pageNumber, pageSize, sortBy, sortDir,CouponDto.class), HttpStatus.OK);
    }
}
