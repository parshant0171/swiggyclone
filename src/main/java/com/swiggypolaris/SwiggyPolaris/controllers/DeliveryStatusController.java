package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.DeliveryStatusDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.DeliveryStatusService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/delivery-statuses")
public class DeliveryStatusController {

    @Autowired
    private DeliveryStatusService deliveryStatusService;

    private Logger logger = LoggerFactory.getLogger(DeliveryStatusController.class);

    // Create Deliver Status
    @PostMapping
    public ResponseEntity<DeliveryStatusDto> create(
            @Valid
            @RequestBody DeliveryStatusDto deliverStatusDto
    ) {
        try{
            DeliveryStatusDto createdDto = deliveryStatusService.create(deliverStatusDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Deliver Status
    @PutMapping("/{id}")
    public ResponseEntity<DeliveryStatusDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody DeliveryStatusDto deliverStatusDto
    ) {
        DeliveryStatusDto updatedDeliveryStatusDto = deliveryStatusService.update(deliverStatusDto, id);
        return new ResponseEntity<>(updatedDeliveryStatusDto, HttpStatus.OK);
    }

    // Delete Deliver Status
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        deliveryStatusService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("Deliver Status is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<DeliveryStatusDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(deliveryStatusService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<DeliveryStatusDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "statusName", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(deliveryStatusService.index(pageNumber, pageSize, sortBy, sortDir,DeliveryStatusDto.class), HttpStatus.OK);
    }
}
