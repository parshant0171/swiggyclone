package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.RiderDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.RiderService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/riders")
public class RiderController {

    @Autowired
    private RiderService riderService;

    private Logger logger = LoggerFactory.getLogger(RiderController.class);

    // Create Rider
    @PostMapping
    public ResponseEntity<RiderDto> create(
            @Valid
            @RequestBody RiderDto riderDto
    ) {
        try{
            RiderDto createdDto = riderService.create(riderDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Rider
    @PutMapping("/{id}")
    public ResponseEntity<RiderDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody RiderDto riderDto
    ) {
        RiderDto updatedRiderDto = riderService.update(riderDto, id);
        return new ResponseEntity<>(updatedRiderDto, HttpStatus.OK);
    }

    // Delete Rider
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        riderService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("Rider is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<RiderDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(riderService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<RiderDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "status", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(riderService.index(pageNumber, pageSize, sortBy, sortDir,RiderDto.class), HttpStatus.OK);
    }
}
