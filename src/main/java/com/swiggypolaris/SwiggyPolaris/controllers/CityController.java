package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.CityDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.CityService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cities")
public class CityController {

    @Autowired
    private CityService cityService;

    private Logger logger = LoggerFactory.getLogger(CityController.class);

    // Create City
    @PostMapping
    public ResponseEntity<CityDto> create(
            @Valid
            @RequestBody CityDto cityDto
    ) {
        try{
            CityDto createdDto = cityService.create(cityDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update City
    @PutMapping("/{id}")
    public ResponseEntity<CityDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody CityDto cityDto
    ) {
        CityDto updatedCityDto = cityService.update(cityDto, id);
        return new ResponseEntity<>(updatedCityDto, HttpStatus.OK);
    }

    // Delete City
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        cityService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("City is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<CityDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(cityService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<CityDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "cityName", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(cityService.index(pageNumber, pageSize, sortBy, sortDir,CityDto.class), HttpStatus.OK);
    }
}
