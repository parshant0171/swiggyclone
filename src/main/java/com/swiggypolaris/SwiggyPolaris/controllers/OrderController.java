package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.OrderDto;
import com.swiggypolaris.SwiggyPolaris.dtos.OrderItemDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.OrderService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    private Logger logger = LoggerFactory.getLogger(OrderController.class);

    // Create Order
    @PostMapping
    public ResponseEntity<OrderDto> create(
            @Valid
            @RequestBody OrderDto orderDto
    ) {
        try{
            OrderDto createdDto = orderService.create(orderDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Order
    @PutMapping("/{id}")
    public ResponseEntity<OrderDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody OrderDto orderDto
    ) {
        OrderDto updatedOrderDto = orderService.update(orderDto, id);
        return new ResponseEntity<>(updatedOrderDto, HttpStatus.OK);
    }

    // Delete Order
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        orderService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("Order is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<OrderDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(orderService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<OrderDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "id", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(orderService.index(pageNumber, pageSize, sortBy, sortDir,OrderDto.class), HttpStatus.OK);
    }

    // Create Order with OrderItem Dto
    @PostMapping("/create-order")
    public ResponseEntity<OrderDto> addOrderItem(@RequestBody OrderItemDto orderItemDto) {
        OrderDto orderDto = orderService.addOrderItem(orderItemDto);
        return new ResponseEntity<>(orderDto, HttpStatus.OK);
    }
}
