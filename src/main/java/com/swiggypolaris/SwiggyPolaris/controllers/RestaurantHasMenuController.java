package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.RestaurantHasMenuDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.RestaurantHasMenuService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/restaurant-has-menus")
public class RestaurantHasMenuController {

    @Autowired
    private RestaurantHasMenuService restaurantHasMenuService;

    private Logger logger = LoggerFactory.getLogger(RestaurantHasMenuController.class);

    // Create Restaurant
    @PostMapping
    public ResponseEntity<RestaurantHasMenuDto> create(
            @Valid
            @RequestBody RestaurantHasMenuDto restaurantHasMenuDto
    ) {
        try{
            RestaurantHasMenuDto createdDto = restaurantHasMenuService.create(restaurantHasMenuDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Restaurant
    @PutMapping("/{id}")
    public ResponseEntity<RestaurantHasMenuDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody RestaurantHasMenuDto restaurantHasMenuDto
    ) {
        RestaurantHasMenuDto updatedRestaurantHasMenuDto = restaurantHasMenuService.update(restaurantHasMenuDto, id);
        return new ResponseEntity<>(updatedRestaurantHasMenuDto, HttpStatus.OK);
    }

    // Delete Restaurant
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        restaurantHasMenuService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("Restaurant is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<RestaurantHasMenuDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(restaurantHasMenuService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<RestaurantHasMenuDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "itemName", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(restaurantHasMenuService.index(pageNumber, pageSize, sortBy, sortDir,RestaurantHasMenuDto.class), HttpStatus.OK);
    }
}
