package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.*;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.RestaurantService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/restaurants")
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    private Logger logger = LoggerFactory.getLogger(RestaurantController.class);

    // Create Restaurant
    @PostMapping
    public ResponseEntity<RestaurantDto> create(
            @Valid
            @RequestBody RestaurantDto restaurantDto
    ) {
        try{
            RestaurantDto createdDto = restaurantService.create(restaurantDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Restaurant
    @PutMapping("/{id}")
    public ResponseEntity<RestaurantDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody RestaurantDto restaurantDto
    ) {
        RestaurantDto updatedRestaurantDto = restaurantService.update(restaurantDto, id);
        return new ResponseEntity<>(updatedRestaurantDto, HttpStatus.OK);
    }

    // Delete Restaurant
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        restaurantService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("Restaurant is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<RestaurantResponseDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(restaurantService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<RestaurantResponseDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "name", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(restaurantService.index(pageNumber, pageSize, sortBy, sortDir,RestaurantDto.class), HttpStatus.OK);
    }

    @GetMapping("/suggest")
    public ResponseEntity<List<SuggestedRestaurantMenuDto>> suggestRestaurants(
            @RequestParam("foodType") String foodType,
            @RequestParam("deliveryTime") int deliveryTimeInMinutes) throws IOException {

        List<SuggestedRestaurantMenuDto> suggestedRestaurants = restaurantService.getSuggestedRestaurants(foodType, deliveryTimeInMinutes);

        return ResponseEntity.ok(suggestedRestaurants);
    }

    @GetMapping("/{restaurantId}/nearest-rider")
    public ResponseEntity<RiderDto> findNearestRider(@PathVariable("restaurantId") Long restaurantId) {
        Optional<RiderDto> riderDto = restaurantService.findNearestRider(restaurantId);
        return riderDto.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }
}