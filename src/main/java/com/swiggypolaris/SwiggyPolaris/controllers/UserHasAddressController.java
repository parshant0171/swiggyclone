package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.UserHasAddressDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.UserHasAddressService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user-has-addresses")
public class UserHasAddressController {

    @Autowired
    private UserHasAddressService userHasAddressService;

    private Logger logger = LoggerFactory.getLogger(UserHasAddressController.class);

    // Create Address of User
    @PostMapping
    public ResponseEntity<UserHasAddressDto> create(
            @Valid
            @RequestBody UserHasAddressDto userHasAddressDto
    ) {
        try{
            UserHasAddressDto createdDto = userHasAddressService.create(userHasAddressDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Address of User
    @PutMapping("/{id}")
    public ResponseEntity<UserHasAddressDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody UserHasAddressDto userHasAddressDto
    ) {
        UserHasAddressDto updatedUserHasAddressDto = userHasAddressService.update(userHasAddressDto, id);
        return new ResponseEntity<>(updatedUserHasAddressDto, HttpStatus.OK);
    }

    // Delete Address of User
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        userHasAddressService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("User's Address is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<UserHasAddressDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(userHasAddressService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<UserHasAddressDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "fullAddress", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(userHasAddressService.index(pageNumber, pageSize, sortBy, sortDir,UserHasAddressDto.class), HttpStatus.OK);
    }
}
