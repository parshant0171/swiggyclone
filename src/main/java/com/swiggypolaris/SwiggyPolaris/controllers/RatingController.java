package com.swiggypolaris.SwiggyPolaris.controllers;

import com.swiggypolaris.SwiggyPolaris.api.ApiResponseMessage;
import com.swiggypolaris.SwiggyPolaris.dtos.RatingDto;
import com.swiggypolaris.SwiggyPolaris.helpers.PageableResponse;
import com.swiggypolaris.SwiggyPolaris.services.CountryService;
import com.swiggypolaris.SwiggyPolaris.services.RatingService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ratings")
public class RatingController {

    @Autowired
    private RatingService ratingService;

    private Logger logger = LoggerFactory.getLogger(RatingController.class);

    // Create Rating
    @PostMapping
    public ResponseEntity<RatingDto> create(
            @Valid
            @RequestBody RatingDto ratingDto
    ) {
        try{
            RatingDto createdDto = ratingService.create(ratingDto);
            return new ResponseEntity<>(createdDto, HttpStatus.CREATED);
        }
        catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    // Update Rating
    @PutMapping("/{id}")
    public ResponseEntity<RatingDto> update(
            @PathVariable("id") long id,
            @Valid @RequestBody RatingDto ratingDto
    ) {
        RatingDto updatedRatingDto = ratingService.update(ratingDto, id);
        return new ResponseEntity<>(updatedRatingDto, HttpStatus.OK);
    }

    // Delete Rating
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponseMessage> delete(
            @PathVariable long id
    ) {
        ratingService.delete(id);
        ApiResponseMessage message
                = ApiResponseMessage
                .builder()
                .message("Rating is deleted Successfully !!")
                .success(true)
                .status(HttpStatus.OK)
                .build();

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    // Get Single Entity By ID
    @GetMapping("/{id}")
    public ResponseEntity<RatingDto> show(
            @PathVariable long id
    ) {
        return new ResponseEntity<>(ratingService.show(id), HttpStatus.OK);
    }

    // Get All the Entities
    @GetMapping
    public ResponseEntity<PageableResponse<RatingDto>> index(
            @RequestParam(value = "pageNumber", defaultValue = "0", required = false) int pageNumber,
            @RequestParam(value = "pageSize", defaultValue = "10", required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = "id", required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = "asc", required = false) String sortDir
    ) {
        return new ResponseEntity<>(ratingService.index(pageNumber, pageSize, sortBy, sortDir,RatingDto.class), HttpStatus.OK);
    }
}
