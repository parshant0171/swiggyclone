package com.swiggypolaris.SwiggyPolaris.helpers;

import com.swiggypolaris.SwiggyPolaris.models.*;
import com.swiggypolaris.SwiggyPolaris.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.Optional;

@Component
public class ApplicationStartup implements CommandLineRunner {

    @Autowired
    private CityRepository cityRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private CouponRepository couponRepository;
    @Autowired
    private DeliveryStatusRepository deliveryStatusRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private RestaurantHasMenuRepository restaurantHasMenuRepository;
    @Autowired
    private RestaurantRepository restaurantRepository;
    @Autowired
    private RiderRepository riderRepository;
    @Autowired
    private StateRepository stateRepository;
    @Autowired
    private UserHasAddressRepository userHasAddressRepository;
    @Autowired
    private UserRepository userRepository;


    @Override
    public void run(String... args) throws Exception {

        try {

            long id = 1;
            Country country = null;
            State state = null;
            City city = null;
            User user = null;
            UserHasAddress userAddress = null;
            Restaurant restaurant = null;
            RestaurantHasMenu restaurantHasMenu = null;

            //Create Country
            Optional<Country> existingCountry = countryRepository.findById(id);

            if(existingCountry.isEmpty()){
                country = Country.builder()
                        .id(1)
                        .countryName("India")
                        .createdAt(new Date())
                        .build();
                countryRepository.save(country);

                System.out.println("Country Created Successfully.");
            }


            //Create State
            Optional<State> existingState = stateRepository.findById(id);

            if(existingState.isEmpty()){
                state = State.builder()
                        .id(1)
                        .country(country)
                        .stateName("Haryana")
                        .createdAt(new Date())
                        .build();

                stateRepository.save(state);

                System.out.println("State Created Successfully.");
            }

            //Create City
            Optional<City> existingCity = cityRepository.findById(id);

            if(existingCity.isEmpty()){
                city = City.builder()
                        .id(1)
                        .state(state)
                        .cityName("Gurgaon")
                        .createdAt(new Date())
                        .build();

                cityRepository.save(city);

                System.out.println("City Created Successfully.");
            }

            //Create User
            Optional<User> existingUser = userRepository.findById(id);

            if(existingUser.isEmpty()){
                user = User.builder()
                        .name("Parshant")
                        .email("parshant0171@gmail.com")
                        .password("ABCD@123##")
                        .mobileNumber(9467940171L)
                        .createdAt(new Date())
                        .build();

                userRepository.save(user);

                System.out.println("User Created Successfully.");
            }

            //Create User's Address
            Optional<UserHasAddress> existingUserAddress = userHasAddressRepository.findById(id);

            if(existingUserAddress.isEmpty()){
                userAddress = UserHasAddress.builder()
                        .id(1)
                        .user(user)
                        .fullAddress("H.No. 678, Vill. Manesar, Gurgaon, Haryana")
                        .city(city)
                        .label("Home")
                        .createdAt(new Date())
                        .build();

                userHasAddressRepository.save(userAddress);

                System.out.println("User Address Created Successfully.");
            }

            //Create Restaurant
            Optional<Restaurant> existingResturant = restaurantRepository.findById(id);

            if(existingResturant.isEmpty()){

                restaurant = Restaurant.builder()
                        .id(1)
                        .name("Haldiram")
                        .contactNumber(9467940121L)
                        .fullAddress("Shop.No. 6218, Vill. Manesar, Gurgaon, Haryana")
                        .city(city)
                        .createdAt(new Date())
                        .build();

                restaurantRepository.save(restaurant);

                System.out.println("Restaurant Created Successfully.");

                //Create Restaurant
                Optional<RestaurantHasMenu> existingRestaurantMenu = restaurantHasMenuRepository.findById(id);

                if(existingRestaurantMenu.isEmpty()) {

                    restaurantHasMenu = RestaurantHasMenu.builder()
                            .id(1)
                            .restaurant(restaurant)
                            .itemName("Dosa")
                            .price(90)
                            .createdAt(new Date())
                            .build();

                    restaurantHasMenuRepository.save(restaurantHasMenu);

                    System.out.println("Restaurant Menu Created Successfully.");

                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
