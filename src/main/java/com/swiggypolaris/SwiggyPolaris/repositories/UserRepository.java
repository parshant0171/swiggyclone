package com.swiggypolaris.SwiggyPolaris.repositories;

import com.swiggypolaris.SwiggyPolaris.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String uniqueUsername);

    Optional<User> findByEmailAndPassword(String uniqueUsername,String password);

    List<User> findByNameContaining(String keywords);
}
