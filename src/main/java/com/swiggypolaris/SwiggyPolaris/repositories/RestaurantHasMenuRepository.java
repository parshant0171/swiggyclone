package com.swiggypolaris.SwiggyPolaris.repositories;

import com.swiggypolaris.SwiggyPolaris.models.RestaurantHasMenu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RestaurantHasMenuRepository extends JpaRepository<RestaurantHasMenu, Long> {

}
