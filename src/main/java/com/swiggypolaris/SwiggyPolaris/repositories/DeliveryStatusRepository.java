package com.swiggypolaris.SwiggyPolaris.repositories;

import com.swiggypolaris.SwiggyPolaris.models.DeliveryStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DeliveryStatusRepository extends JpaRepository<DeliveryStatus, Long> {
    // To get the Status by using Status Name
    Optional<DeliveryStatus> findByStatusName(String statusName);

}
