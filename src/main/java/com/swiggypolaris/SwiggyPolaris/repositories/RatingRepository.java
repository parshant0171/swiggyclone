package com.swiggypolaris.SwiggyPolaris.repositories;

import com.swiggypolaris.SwiggyPolaris.models.Rating;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RatingRepository extends JpaRepository<Rating, Long> {

}
