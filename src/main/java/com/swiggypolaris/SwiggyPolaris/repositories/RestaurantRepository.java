package com.swiggypolaris.SwiggyPolaris.repositories;

import com.swiggypolaris.SwiggyPolaris.models.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
    @Query("SELECT r.id, r.name, r.contactNumber, r.fullAddress, m.id, m.itemName, m.price, m.cookingTime " +
            "FROM Restaurant r JOIN r.restaurantMenus m " +
            "WHERE m.itemName = :itemName AND m.cookingTime < :cookingTime")
    List<Object[]> findByFoodTypeAndDeliveryTime(@Param("itemName") String itemName, @Param("cookingTime") int cookingTime);

}
