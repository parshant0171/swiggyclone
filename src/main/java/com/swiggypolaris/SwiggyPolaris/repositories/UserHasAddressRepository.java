package com.swiggypolaris.SwiggyPolaris.repositories;

import com.swiggypolaris.SwiggyPolaris.models.UserHasAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserHasAddressRepository extends JpaRepository<UserHasAddress, Long> {

}
