package com.swiggypolaris.SwiggyPolaris.repositories;

import com.swiggypolaris.SwiggyPolaris.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    // To get the Order which is not accepted and created by a user
    @Query("SELECT o FROM Order o WHERE o.user.id = :userId AND o.isAccepted = false")
    Optional<Order> findByUserIdAndIsAcceptedFalse(@Param("userId") long userId);
}
