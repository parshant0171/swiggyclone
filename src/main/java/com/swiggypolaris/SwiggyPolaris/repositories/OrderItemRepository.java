package com.swiggypolaris.SwiggyPolaris.repositories;

import com.swiggypolaris.SwiggyPolaris.models.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

}
