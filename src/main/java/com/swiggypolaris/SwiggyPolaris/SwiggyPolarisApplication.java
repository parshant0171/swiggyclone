package com.swiggypolaris.SwiggyPolaris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwiggyPolarisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwiggyPolarisApplication.class, args);
	}

}
