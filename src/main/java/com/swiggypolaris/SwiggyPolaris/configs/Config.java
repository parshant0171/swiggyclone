package com.swiggypolaris.SwiggyPolaris.configs;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
public class Config {
    @Bean
    public ModelMapper mapper() {
        return new ModelMapper();
    }

}
