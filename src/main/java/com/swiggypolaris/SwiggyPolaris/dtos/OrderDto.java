package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderDto {

    private long id;

    private long userId;

    private long restaurantId;

    private double orderTotal;

    private long deliveryStatusId;

    private long riderId;

    private boolean isAccepted;

    private long estimatedDeliveryTime;

    private long actualDeliveryTime;

    private Date createdAt;

    private Date updatedAt;

}
