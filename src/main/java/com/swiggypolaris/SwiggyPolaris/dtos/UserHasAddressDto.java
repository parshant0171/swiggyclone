package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserHasAddressDto {

    private long id;

    private long userId;

    @NotBlank(message = "Address can't be blank !!")
    private String fullAddress;

    private long cityId;

    @NotBlank(message = "Label can't be blank !!")
    private String label;

    private Date createdAt;

    private Date updatedAt;

}
