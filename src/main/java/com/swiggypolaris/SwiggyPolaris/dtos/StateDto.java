package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StateDto {

    private long id;

    private long countryId;

    @NotBlank(message = "State Name can't be blank !!")
    private String stateName;

    private Date createdAt;

    private Date updatedAt;

}