package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RestaurantHasMenuDto {

    private long id;

    private long restaurantId;

    @NotBlank(message = "Item Name name can't be blank !!")
    private String itemName;

    @NotBlank(message = "Price can't be blank !!")
    private double price;

    @NotBlank(message = "Cooking Time can't be blank !!")
    private int cookingTime;

    private Date createdAt;

    private Date updatedAt;

}
