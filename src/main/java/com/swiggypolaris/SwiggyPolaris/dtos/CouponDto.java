package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CouponDto {

    private long id;

    @NotBlank(message = "Code can't be blank !!")
    private String code;

    @Min(0)
    @Max(100)
    @NotBlank(message = "Discount Percentage can't be blank !!")
    private Integer discountPercentage;

    private Date expirationDate;

    private Date createdAt;

    private Date updatedAt;

}
