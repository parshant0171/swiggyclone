package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderItemDto {

    private long id;

    private long orderId;

    private long restaurantHasMenuId;

    @NotNull(message = "Quantity can't be blank !!")
    private Integer quantity;

    private Date createdAt;

    private Date updatedAt;

}
