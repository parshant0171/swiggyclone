package com.swiggypolaris.SwiggyPolaris.dtos;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuggestedRestaurantMenuDto {
    private long restaurantId;
    private String restaurantName;
    private long contactNumber;
    private String fullAddress;
    private long menuId;
    private String itemName;
    private double price;
    private int cookingTime;
}
