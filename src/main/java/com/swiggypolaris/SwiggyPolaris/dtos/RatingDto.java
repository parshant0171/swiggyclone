package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RatingDto {

    private long id;

    private long orderId;

    private long userId;

    private long riderId;

    @NotBlank(message = "Rating can't be blank !!")
    private int rating;

    private String comment;

    private Date createdAt;
}
