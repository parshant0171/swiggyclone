package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CountryDto {

    private long id;

    @NotBlank(message = "Country Name can't be blank !!")
    private String countryName;

    private Date createdAt;

    private Date updatedAt;

}