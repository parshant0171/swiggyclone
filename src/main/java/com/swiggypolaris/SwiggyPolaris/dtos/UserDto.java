package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {

    private long id;

    @NotBlank(message = "Name can't be blank !!")
    private String name;

    @NotBlank(message = "Email can't be blank !!")
    @Email(message = "Invalid email format for Email !!")
    private String email;

    @NotBlank(message = "Password can't be blank !!")
    @Size(min = 8, max = 20, message = "Password length must be between 5 and 12 characters !!")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]+$",
            message = "Password must contain at least one uppercase letter, one lowercase letter, one digit, and one special character !!")
    private String password;

    @Digits(integer = 10, fraction = 0, message = "Mobile number must be numeric and have at most 10 digits")
    private long mobileNumber;

    private Date createdAt;

    private Date updatedAt;

}
