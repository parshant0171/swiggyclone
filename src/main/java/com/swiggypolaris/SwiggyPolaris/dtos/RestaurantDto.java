package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RestaurantDto {

    private long id;

    @NotBlank(message = "Restaurant name can't be blank !!")
    private String name;

    @Digits(integer = 10, fraction = 0, message = "Contact number must be numeric and have at most 10 digits")
    private long contactNumber;

    @NotBlank(message = "Address can't be blank !!")
    private String fullAddress;

    @NotBlank(message = "Current Location Longitude can't be blank !!")
    private double currentLocationLongitude;

    @NotBlank(message = "Current Location Latitude can't be blank !!")
    private double currentLocationLatitude;

    private long cityId;

    private Date createdAt;

    private Date updatedAt;
}
