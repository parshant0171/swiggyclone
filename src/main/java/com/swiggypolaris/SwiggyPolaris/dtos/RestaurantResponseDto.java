package com.swiggypolaris.SwiggyPolaris.dtos;

import com.swiggypolaris.SwiggyPolaris.models.City;
import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RestaurantResponseDto {

    private long id;

    @NotBlank(message = "Restaurant name can't be blank !!")
    private String name;

    @Digits(integer = 10, fraction = 0, message = "Contact number must be numeric and have at most 10 digits")
    private long contactNumber;

    @NotBlank(message = "Address can't be blank !!")
    private String fullAddress;

    private CityDto city;

    private List<RestaurantHasMenuDto> menus;

    private Date createdAt;

    private Date updatedAt;
}
