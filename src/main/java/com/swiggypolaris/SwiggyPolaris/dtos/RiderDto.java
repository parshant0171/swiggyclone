package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.persistence.Column;
import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RiderDto {

    private long id;

    private long userId;

    @Digits(integer = 10, fraction = 0, message = "Mobile number must be numeric and have at most 10 digits")
    private long mobileNumber;

    @NotBlank(message = "Current Location Longitude can't be blank !!")
    private double currentLocationLongitude;

    @NotBlank(message = "Current Location Latitude can't be blank !!")
    private double currentLocationLatitude;

    @NotBlank(message = "Status can't be blank !!")
    private String status;

    private Date createdAt;

    private Date updatedAt;

}
