package com.swiggypolaris.SwiggyPolaris.dtos;

import lombok.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserResponseDto {

    private long id;

    private String name;

    private String email;

    private String password;

    private long mobileNumber;

    private List<UserHasAddressDto> addresses;

    private List<OrderDto> orders;

    private Date createdAt;

    private Date updatedAt;

}
