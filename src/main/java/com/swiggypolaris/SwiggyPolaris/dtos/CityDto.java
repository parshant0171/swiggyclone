package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CityDto {

    private long id;

    private long stateId;

    @NotBlank(message = "City Name can't be blank !!")
    private String cityName;

    @NotBlank(message = "Pincode can't be blank !!")
    private long pincode;

    private Date createdAt;

    private Date updatedAt;

}