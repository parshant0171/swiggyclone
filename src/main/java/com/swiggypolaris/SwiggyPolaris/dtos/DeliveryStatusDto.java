package com.swiggypolaris.SwiggyPolaris.dtos;

import jakarta.validation.constraints.*;
import lombok.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeliveryStatusDto {

    private long id;

    @NotBlank(message = "Status Name name can't be blank !!")
    private String statusName;

    private Date createdAt;

    private Date updatedAt;

}
